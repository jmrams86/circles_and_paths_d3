# About

A animation using d3.js (v4) of a solution to the three body problem.

I modelled the movement of the bodies in python using this cool [post](https://towardsdatascience.com/modelling-the-three-body-problem-in-classical-mechanics-using-python-9dc270ad7767) as a guide. 

The results of that are in the csv. There after d3 takes care of the rest.
