// import the csv dataset and run code inside of this
d3.csv("data.csv")

    // parse the data in proper datatypes
    .row(function(d){ return {
      body:Number(d.body),
      x:Number(d.x),
      y:Number(d.y),
      z:Number(d.z)};})

    // Now work with the data
    .get(function(error,data){

        // Set the plot dimensions
        var height = window.innerHeight||document.documentElement.clientHeight||document.body.clientHeight;
        var width = window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth;

        // Initialise the svg canvas
        var svg = d3.select("body")
                    .append("svg")
                    .attr("height","100%")
                    .attr("width","100%");

        // set up line/body colours
        var colours =['#99cc99',
                      '#ffc0cb',
                      '#ffff7f'];

        // set up line stroke
        var stroke = width/4000

        // Set up line function
        var line = d3.line()
                        .x(function(d,i){ return (d.x * (width/6))+(width/2); })
                        .y(function(d,i){ return (d.y * (height/3))+(height/2); })
                        .curve(d3.curveNatural);

        // Set up transition functions
        function tweenDash() {
      			var l = this.getTotalLength(),
      				  i = d3.interpolateString("0," + l, l + "," + l);
            return function (t) { return i(t); };
    		}

        function tweenCircle(i, paths) {
          var path = paths
            .filter(function(_, j) { return i === j; })
            .node();
          var l = path.getTotalLength();
          return function(t) {
            var p = path.getPointAtLength(t * l);
            return "translate(" + [p.x, p.y] + ")";
          };
        }

        function transition(path, circle) {
          path.transition()
            .duration(35000)
            .attrTween("stroke-dasharray", tweenDash)
            .ease(d3.easeLinear);

          circle.transition()
            .duration(35000)
            .attrTween("transform", function(d, i) { return tweenCircle(i, path); })
            .attr("r", function (d, i) { return ((d.values[i].z + 2) * (height/1200)) + (height/800); })
            .ease(d3.easeLinear);
        }

        // Group the data by "body"
        var dataGroup = d3.nest()
                          .key(function(d){ return d.body; })
                          .entries(data);

        // Create the paths
        var path = svg.selectAll("path")
                      .data(dataGroup)
                      .enter()
                      .append("path")
                      .attr("d", function(d) { return line(d.values); })
                      .attr("stroke", function(d, i) { return colours[i]; })
                      .attr("stroke-width", stroke)
                      .attr("fill", "none");

        // Create the body circles
        var circle = svg.selectAll("circle")
                        .data(dataGroup)
                        .enter()
                        .append("circle")
                        .attr("fill", function(d, i) { return colours[i]; })
                        .attr("id", function(d, i) { return "body"+i; })
                        .attr("transform", function(d) {
                          const start = d.values[0];
                          return "translate(" + [start.x, start.y] + ")"; })
                        .attr("r", function(d, i) {
                          const start = d.values[0];
                          return ((start.z+2) * (height/450))+(height/300); });

        // Call the transtions to start the animation
        transition(path, circle);

        // Highlight when mouseover
        svg.selectAll("path")
                  .on("mouseover", function () { d3.select(this).style("stroke-width", stroke*2);})
                  .on("mouseout", function () { d3.select(this).style("stroke-width", stroke);});

    })
